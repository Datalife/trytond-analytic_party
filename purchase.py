# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta, Pool
from trytond.model import fields


class PurchaseLine(metaclass=PoolMeta):
    __name__ = 'purchase.line'

    @fields.depends('purchase', 'analytic_accounts', methods=[
        'get_party_analytic_account_domain'])
    def on_change_purchase(self):
        pool = Pool()
        AnalyticAccount = pool.get('analytic_account.account')

        try:
            super().on_change_purchase()
        except AttributeError:
            pass
        if self.purchase:
            for entry_acc in self.analytic_accounts:
                accounts = AnalyticAccount.search(
                    self.get_party_analytic_account_domain(entry_acc.root))
                if accounts and not getattr(entry_acc, 'account', None):
                    entry_acc.account = accounts[0]

    @fields.depends('purchase', '_parent_purchase.company',
        '_parent_purchase.party')
    def get_party_analytic_account_domain(self, root):
        return [
            ('root', '=', root),
            ('company', '=', self.purchase.company),
            ('parties', '=', self.purchase.party.id)
        ]
