=======================
Analytic Party Scenario
=======================

Imports::

    >>> from decimal import Decimal
    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.currency.tests.tools import get_currency
    >>> from trytond.modules.account.tests.tools import \
    ...     create_chart, get_accounts


Install analytic_party::

    >>> config = activate_modules(['analytic_party', 'analytic_sale',
    ...     'analytic_purchase', 'analytic_invoice'])


Create companies::

    >>> Party = Model.get('party.party')
    >>> Company = Model.get('company.company')
    >>> _ = create_company()
    >>> company = get_company()
    >>> party_company = Party(name='Party Company')
    >>> party_company.save()
    >>> _ = create_company(party=party_company)
    >>> company2, = Company.find([('party', '=', party_company)])


Create chart of accounts::

    >>> _ = create_chart(company)
    >>> accounts = get_accounts(company)
    >>> revenue = accounts['revenue']


Create Party::

    >>> party = Party(name='Party')
    >>> party.save()
    >>> Conf = Model.get('party.configuration')
	>>> conf = Conf(1)
	>>> conf.force_analytic_code_numeric = True
	>>> conf.save()


Create account category::

    >>> ProductCategory = Model.get('product.category')
    >>> account_category = ProductCategory(name="Account Category")
    >>> account_category.accounting = True
    >>> account_category.account_revenue = revenue
    >>> account_category.save()


Create Product::

    >>> ProductUom = Model.get('product.uom')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> ProductTemplate = Model.get('product.template')
    >>> template = ProductTemplate()
    >>> template.name = 'product'
    >>> template.default_uom = unit
    >>> template.type = 'service'
    >>> template.list_price = Decimal('40')
    >>> template.salable = True
    >>> template.purchasable = True
    >>> template.account_category = account_category
    >>> template.save()
    >>> product, = template.products


Get user::

    >>> User = Model.get('res.user')
    >>> user = User(config.user)
    >>> user.company_filter = 'all'
    >>> user.companies.append(company2)
    >>> company2 = Company(company2.id)
    >>> user.save()


Create analytic accounts::

    >>> AnalyticAccount = Model.get('analytic_account.account')
    >>> root = AnalyticAccount()
    >>> root.name = 'Root'
    >>> root.code = '1'
    >>> root.type = 'root'
    >>> root.company == company
    True
    >>> root.save()
    >>> root2 = AnalyticAccount()
    >>> root2.name = 'Root 2'
    >>> root2.code = '2'
    >>> root2.type = 'root'
    >>> root2.company = company2
    >>> root2.save()
    >>> analytic_account = AnalyticAccount()
    >>> analytic_account.name = 'Analytic 1'
    >>> analytic_account.code = '11A'
    >>> analytic_account.root = root
    >>> analytic_account.parent = root
    >>> analytic_account.save()
    >>> analytic_account2 = AnalyticAccount()
    >>> analytic_account2.name = 'Analytic 2'
    >>> analytic_account2.code = '12'
    >>> analytic_account2.root = root
    >>> analytic_account2.parent = root
    >>> analytic_account2.save()
    >>> analytic_account3 = AnalyticAccount()
    >>> analytic_account3.name = 'Analytic 3'
    >>> analytic_account3.code = '21'
    >>> analytic_account3.root = root2
    >>> analytic_account3.parent = root2
    >>> analytic_account3.company = company2
    >>> analytic_account3.save()


Add analytic account to party with invalid code::

    >>> analytic_account = AnalyticAccount(analytic_account.id)
    >>> party.analytic_accounts.append(analytic_account)
    >>> party.save()
    Traceback (most recent call last):
        ...
    trytond.exceptions.UserError: Code of analytic account "11A - Analytic 1" must be numeric. - 
    >>> analytic_account.code = '11'
    >>> analytic_account.save()
    >>> party.reload()


Add analytic accounts to party::

    >>> user.company_filter = 'one'
    >>> user.save()
    >>> config._context = User.get_preferences(True, {})
    >>> analytic_account = AnalyticAccount(analytic_account.id)
    >>> party.analytic_accounts.append(analytic_account)
    >>> party.analytic_accounts.append(analytic_account2)
    >>> party.save()
    >>> party.analytic_accounts.append(analytic_account3)
    >>> party.save()
    Traceback (most recent call last):
        ...
    trytond.model.modelstorage.AccessError: You are not allowed to create records of "Party - Analytic Account" because they fail on at least one of these rules:
    User in companies - 
    >>> party.reload()
    >>> len(party.analytic_accounts)
    2


check invalid code on party account::

    >>> analytic_account = AnalyticAccount(analytic_account.id)
    >>> analytic_account.code = "11A"
    >>> analytic_account.save()
    Traceback (most recent call last):
        ...
    trytond.exceptions.UserError: Code of analytic account "11A - Analytic 1" must be numeric. - 


Create Sale::

    >>> Sale = Model.get('sale.sale')
    >>> sale = Sale()
    >>> sale.party = party
    >>> sline = sale.lines.new()
    >>> sline.product = product
    >>> sline.quantity = 2.0
    >>> entry_acc, = sline.analytic_accounts
    >>> entry_acc.account == analytic_account
    True
    >>> sale.save()


Create Purchase::

    >>> Purchase = Model.get('purchase.purchase')
    >>> purchase = Purchase()
    >>> purchase.party = party
    >>> pline = purchase.lines.new()
    >>> pline.product = product
    >>> pline.quantity = 2.0
    >>> entry_acc, = pline.analytic_accounts
    >>> entry_acc.account == analytic_account
    True
    >>> purchase.save()


Create Invoice::

    >>> Invoice = Model.get('account.invoice')
    >>> invoice = Invoice()
    >>> invoice.party = party
    >>> iline = invoice.lines.new()
    >>> iline.product = product
    >>> iline.quantity = 2.0
    >>> iline.unit_price = Decimal('5.0')
    >>> entry_acc, = iline.analytic_accounts
    >>> entry_acc.account == analytic_account
    True
    >>> invoice.save()


Add analytic accounts to party on company2::

    >>> user.company = Company(company2.id)
    >>> user.save()
    >>> config._context = User.get_preferences(True, {})
    >>> party.reload()
    >>> len(party.analytic_accounts)
    0
    >>> analytic_account = AnalyticAccount(analytic_account.id)
    >>> analytic_account3 = AnalyticAccount(analytic_account3.id)
    >>> party.analytic_accounts.append(analytic_account)
    >>> party.save()
    Traceback (most recent call last):
        ...
    trytond.model.modelstorage.AccessError: You are not allowed to create records of "Party - Analytic Account" because they fail on at least one of these rules:
    User in companies - 
    >>> party.reload()
    >>> party.analytic_accounts.append(analytic_account3)
    >>> party.save()
    >>> len(party.analytic_accounts)
    1