datalife_analytic_party
=======================

The analytic_party module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-analytic_party/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-analytic_party)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
